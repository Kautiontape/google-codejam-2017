#!/usr/bin/env python

import sys
import math

def LS(s):
    return math.ceil(s / 2) - 1

def RS(s):
    return math.floor(s / 2)

def iteratechain(root, chain):
    s = root
    for c in chain:
        if c == 'LS':
            s = LS(s)
        else:
            s = RS(s)
    return s

def parent(n):
    return n / 2 if n % 2 == 0 else (n - 1) / 2

def genchain(node):
    chain = []
    n = node
    while n > 1:
        if n % 2 == 0:
            chain.append('RS')
        else:
            chain.append('LS')
        n = parent(n)

    return chain

def solve(N, K):
    s = iteratechain(N, genchain(K))
    return LS(s), RS(s)


def main(infile):
    with open(infile, 'r') as f:
        T = f.readline()

        case = 1
        for line in f:
            s = list(map(int, line.split(' ')))
            l, r = solve(s[0], s[1])
            print("Case #{}: {} {}".format(case, max(l, r), min(l, r)))
            case = case + 1

if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
