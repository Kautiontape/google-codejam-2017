#!/usr/bin/env python

import sys


def solve():
    pass

def main(infile):
    with open(infile, 'r') as f:
        T = f.readline()

        case = 1
        for line in f:
            r = solve()
            print("Case #{}: {}".format(case, r))
            case = case + 1

if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
