#!/usr/bin/env python

import sys
from itertools import groupby

def expand(p):
    return ''.join([k[0]*k[1] for k in p])

def compress(p):
    return [(k, sum(1 for i in g)) for k, g in groupby(p)]

def indexv(p, v):
    return [k for k,j in filter(lambda x: x[1][0] == v, enumerate(p))][0]

def indexn(p):
    return indexv(p, '-')

def indexp(p):
    return indexv(p, '+')

def r(p, i):
    if len(p) == 0:
        return 0

    r(tail, i)

def solve(pancakes, K):
    print("Solving {} with {} flipper".format(pancakes, K))
    p = compress(pancakes)

def main(infile):
    with open(infile, 'r') as f:
        T = f.readline()

        for line in f:
            s = line.split(' ')
            solve(s[0], int(s[1]))

if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
